package main

import (
	"log"
	"sort"
)

type User struct {
	FirstName string
	LastName string
}
func main() {

	// var myOtherMap map[string]string - you can use it this way too, but it's NEVER used like that.
	
	myMap := make(map[string]string)

	myMap["dog"] = "Samson"

	log.Println(myMap["dog"])

	myMap["other-dog"] = "Cassie"
	log.Println(myMap["other-dog"])

	myMap["dog"] = "fido"
	log.Println(myMap["dog"])

	myMap2 := make(map[string]int)

	myMap2["Age"] = 32
	myMap2["Born"] = 1988

	log.Println(myMap2["Age"], myMap2["Born"])

	myMap3 := make(map[int]bool)

	myMap3[1] = true
	myMap3[2] = false

	log.Println(myMap3[1], myMap3[2])

	// -------------------------------------- //

	myMap4 := make(map[string]User)

	me := User {
		FirstName: "Jane",
		LastName: "Zdravevski",
	}

	myMap4["myself"] = me

	log.Println(myMap4["myself"], myMap4["myself"].FirstName, myMap4["myself"].LastName )

	var myNewVar float32 = 11.1
	log.Println(myNewVar)

/* maps are immutable / constant. If you going to use something like the myNewVar like above, a variable
that holds a value of version, and you are using that all over your program. Then you want to add a pointer
to that value, so whenever you are changing it, you don't change the variable, but the memory slot value,
so it is affecting the whole program, or if somewhere else the version is getting changed, you want
the places where you are using it, to be affected by the change too.
With MAPS, no need to assign a pointer, since you know the map is immutable. It's a constant.
So instead of making a pointer, you can send the map itself. And they are VERY FAST!
Maps are also not sorted. So you can't access with index number or anything. YOU HAVE To access them
by the key you assign to them.You save them in one order, you can't expect them to be in the same
order when you try to retrieve them, therefore no indexes or anything.*/

	myMap6 := make(map[string]interface{}) // interface{} is used when you don't know
	// what value you wanna store inside. IT IS NOT recommended tho.

	myMap6["One"] = 1
	myMap6["Two"] = "Too"
	log.Printf("%T\n",myMap6["One"])
	log.Printf("%T\n",myMap6["Two"]) // This returns integer, meaning that interface{} takes the actual type that you are giving.
	log.Println(myMap6)

	// -------------------_ SLICES ------------------------ //

	var myString = "Fish"
	log.Println(myString)

	var mySlice []string
	mySlice = append(mySlice,"One")
	mySlice = append(mySlice,"Two")
	mySlice = append(mySlice,"Three")
	log.Println(mySlice)
	var mySliceInts []int
	mySliceInts = append(mySliceInts, 2)
	mySliceInts = append(mySliceInts, 3)
	mySliceInts = append(mySliceInts, 1)
	log.Println(mySliceInts) // outputs [2 3 1]
	log.Println(mySliceInts[0]) // so index starts from 0 too. Nice - Outputs 2
	// Slices are orderable. Basically arrays in order languages, just better.

	sort.Ints(mySliceInts) //package for sorting stuffs. Here it sorted the ints.
	log.Println(mySliceInts) //outputs [1 2 3]
	log.Println(mySliceInts[0]) // Outputs 1 - After the sort function now 1 takes the first spot / index0

	numbers := []int{0,1,2,3,4,5,6,7,8,9}
	log.Println(numbers)
	log.Println(numbers[0:2]) // Prints [0 1]. Starts at 0 (prints index 0 too) ends at 2 (doesn't print index 2)
	log.Println(numbers[5:9]) // Prints [5,6,7,8]

	names := []string{"One", "Seven", "Fish", "Cats"}
	log.Println(names)
}
/* var myString string
	var myInt int

	myString = "hi"
	myInt = 11

	mySecondString := "another string"

	log.Println(myInt, myString, mySecondString) */