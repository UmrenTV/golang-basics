package main

import "log"

// var s string another way of declaring and initiating in the same time of variable. It recognizes the type like this.
var s = "seven"

func main() {
	var s2 = "six"

	// s := "eight" // with := we are basically saying "Take the variable on the left, and match the type and the value to whatever is on the right (type: string, value: "eight")"
	// also, same as below, we don't really declare same-name variables as the global ones, because that is called Variable Shadowing. You won't be able to use the global
	// variable named "s" if you name a local "s" variable. It will always prioritize the local variables.

	log.Println("s is", s)
	log.Println("s2 is", s2)

	saySomething("xxx")
}

/* func saySomething (s string) (string, string) {
	log.Println("s from the saySomething func is", s)
	return s, "World"
} if we try to call this function like this, it has name collision s is mentioned here, and it's set as global variable above as "seven".
When we call it, it prints out this as xxx, which means that if we use a variable with the same name inside function like this, we cannot use
the global variable in that function again. (In VUE there is something called this.s and then you refer to the global s, here no way, yet)
So it's good if we change the variables like the below example to something like s2 or s3 or whatever. */

func saySomething (s3 string) (string, string) {
	log.Println("s from the saySomething func is", s)
	return s, "World"
} // Now this is different since the value we accepted as argument, we have put into a variable named s3, which is not taken, therefore no clashin'.