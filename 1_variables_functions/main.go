package main

import "log"

func main() {
	var whatToSay string
	var saySomethingElse string
	var i int


	whatToSay, _ = saySomething("yo")
	// if you have function that returns 2 values, you have to either save the other value, or ignore it with _. Otherwise = error.

	log.Println(whatToSay)

	saySomethingElse, _ = saySomething("Goodbye")

	log.Println(saySomethingElse)

	log.Println(saySomething("Finally"))

	i = 7
	i = 8

	log.Println(i)
}

/* func saySomething(s string) string {
	return s
} The function below, has two outputs */

func saySomething(s string) (string, string) {
	return s, "world"
}