package main

import "fmt"

type myStruct struct {
	FirstName string
}
// so Here we are assigning to this function, a receiver (is what is called) which ties
// the type of this function, to the type of this Scruct (myStruct) and that allows us
// access to information from myStruct
func (m *myStruct) printFirstName() string {
	return m.FirstName
// this function is considered to be part of this struct. Since only thing it does, it's
// allowing access to that struct, and doing shenenigans with some code inside. Here we are just
// returning the firstName. You can of course some complex business logic that will run when
// you call this receiver, that is extremely useful, and it's part of this struct.
}

func main() {
	var myVar myStruct
	myVar.FirstName = "Jane"

	myVar2 := myStruct{
		FirstName: "John",
	}

	// myVar3 := "Bob"
	fmt.Println(myVar.FirstName, myVar2.FirstName)
	fmt.Println(myVar2.printFirstName())
	// fmt.Println(myVar3.printFirstName()) - This will not work, since the Var3 is string
	// and string, as a type/struct, has no methods or fields called printFirstName associated with it.
}