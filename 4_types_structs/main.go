package main

import (
	"log"
	"time"
)

//the difference between defining a variable with capital, vs non-capital letters is that when used capital letters, you are setting those variables as Global
// something like "public" in Java. Which means you can call those functions/variables from outside the package you are working on right now (main in our case)
// That is why below we got User and FirstName LastName capitalized. Also if you type log. and see the suggestions, you'll see all the outside functions are capitalized.

type User struct {
	FirstName string
	LastName string
	PhoneNumber string
	Age int
	BirthDate time.Time
}

// var special string // this variable is private, which means it's only available to this particular package. non-capitalized

// var Special string // this on the other hand is visible outside the current package, and it's considered in other programming languages as public.


/* 	var firstName string
	var lastName string
	var phoneNumber string
	var age int
	var birthDate time.Time 
// The point is, instead of having a function that accepts million arguments (looks ugly, it's complicated to maintain),
// you can create a type, that you can call User for example, and use struct (which means structure), and you are saying what structure it will have. */

func main() {
	user := User {
		FirstName: "Trevor",
		LastName: "Sawler",
		PhoneNumber: "1 555 555 555",
	}
	log.Println(user.FirstName, user.LastName, "Age:", user.Age)
}