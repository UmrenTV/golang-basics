package main

import "log"

func main() {
	var myString string
	myString = "Green"


	log.Println("myString is set to", myString)
	changeUsingPointer(&myString)
	// the & sign before a variable, means that we are not sending the actual variable VALUE but the variable MEMORY LOCATION, or reference to that variable (where it's saved.)
	log.Println("after func call myString is set to", myString)


}

func changeUsingPointer(s *string) {
	log.Println("s is set to", s)
	newValue := "Red"
	*s = newValue
	log.Println("s is set to", *s)
}

// So what we are doing here with the (s *string) basically means is we are not really accepting a string, but a reference to a string.
// You basically have to say at what memory slot is the argument you are passing saved. As you can see, we are calling this function with the & sign above, which means
// that we are just saying: "Yo, function, do whatever you wanna do, and here is where the value you need is saved.". That allows us to communicate with the memory slot
// where the variable is saved in the RAM/HDD directly. Then in the func we are creating a new variable newValue and we are saying yo... you remember that *string reference
// we had as argument up there? Well we saved that as "s" right? Now in order to assign a new value to that reference or memory slot, you need to use * before the s variable.
// so *s = newValue is exactly that. We are just applying the value we saved above "Red" in this case, to that memory slot. If we want to print the memory location, we can print it just with
// using "s" as you can see in the example. IF we did it with *s it will print the value instead.
// THE MAIN point here, is also that we have changed a variable that is eclosed by the main function, which makes it (usually) out of our scope, if we wanna change it from
// within other function, but since we are using Pointers, we can access, not to the variable, but to the memory slot directly where that variable is saved. Which is SUPER!