package main

import "log"

func main() {
	var isTrue bool
	isTrue = true

	if isTrue == true { // this is same as saying if isTrue {}
		log.Println("true is", isTrue)

	} else {
		log.Println("false is", isTrue)}

		cat := "cat2"

		if cat == "cat" {
			log.Println("Cat is", cat)
		} else {
			log.Println("cat is not a cat")
		}

		/* myNum := 100
		isTrue2 := false

		if myNum > 99 && !isTrue2 {
			log.Println("myNum is greater than 99 and isTrue is set to true")
		} else if myNum < 100 && isTrue2 {

		} else if myNum == 101 || isTrue2 {

		} else if myNum > 1000 && !isTrue2 {

		}
		You generally never do an if statement if the conditions are larger than 1 or 2.
		You never wanna do like 10 else if statements in a chain. There are better options for that called Switches */
		myVar5 := "meh"
		switch myVar5 {
		case "cat": 
		log.Println("cat is set to cat")

		case "dog":
			log.Println("dog is set to dog")

		case "fish":
			log.Println("fish is set to fish")

		default:
			log.Println("Default")
		}
		// This concept is not that hard to understand. you set the variable you want to test as a switch,
		// and then you saying... for the case when this variable is "cat": do this. - you need colon after the case, and new line
		// the default case at the end, basically means "else", or if nothing of the above is true, then make default happen.
		// WORTH NOTING: is that Go is not like the other languages where if you have multiple true cases (for some reason), all of them
		// will execute. In Golang, that doesn't happen. The first case that will happen to match, it gets out of the loop instantly.



		

}