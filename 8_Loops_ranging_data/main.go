package main

import "log"

type User struct {
	FirstName string
	}

func main() {
	j := 15
	for i := 0; i <= j; i++ {
		log.Println(i)
	} // loops

	mySlice := []string{"dog", "cat", "horse", "fish", "banana"}

	for _, x := range mySlice {
		log.Println(x)
	} // So basically what a range is almost the same as loop. The _ we have to use, and usually is index
	// but in this case we omit that with underscore.
	myMap := make(map[string]string)
		myMap["dog"] = "dog"
		myMap["fish"] = "fish"
		myMap["cat"] = "cat"
		myMap["horse"] = "horse"
		myMap["banana"] = "banana"
	

	for i, x := range myMap {
		log.Println(i, x)
	} /* So this way we are cycling through the range of the map, while we also use the first argument "i".
	apparently it takes the value of the (map[STRING]string) STRING that we use as index, and it prints out that
	too in the print command. Another note, if you run the program multiple times, you'll notice that the output order
	is always different. we get dog fish cat banana, next time cat banana fish dog. Map is un-ordered and intentionally randomized!
	*/
		
		var slice2 []User // define slice2 as a slice with type of data: User (we made custom type above named User that needs FirstName)

		u1 := User{ // we creating a variable with type User named u1, and filling the FirsName field with "Trevor"
			FirstName: "Trevor",
		}
		u2 := User{FirstName: "Sam",} // doing the same with u2
		slice2 = append(slice2, u1) // we are appending the u1 User type variable, into the slice that should hold User type entries
		slice2 = append(slice2, u2) // also the u2

		for i, x := range slice2 { // now it's pretty much the same as map. It goes through the whole slice
			log.Println(i, x) // printing the index, and then the element at that index. Here is {Trevor} and {Sam}
			log.Println(i, x.FirstName) // Like this we can print the actual value of that element, for the index i. Here is Trevor and Sam

		}

		// Quick definition for range: "For i (index), give me value at that index and set it as x with := inheriting the type, through
		// the whole range of this variable (slice2 in our case above). As long as the variable is rangeable (has more elements) this
		// will loop through all of the elements until there are none left. (hint: string, for example, is not loop-able)
		



}